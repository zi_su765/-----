﻿using UnityEngine;
using System.Collections;

public class TitleHand : MonoBehaviour {

	HandController handController;
	bool isGrab = true;
	bool isGrabOld = true;
	bool isEnd = false;
	public VoiceManager vm;
	GameObject voice;
	// Use this for initialization
	void Start () {
		handController = GetComponent<HandController>();
		isGrab = false;
	}
	
	// Update is called once per frame
	void Update () {
		Leap.Frame frame = handController.GetFrame();
		if(frame != null){
			foreach(Leap.Hand h in frame.Hands){
				Debug.Log ("Grab:"+h.GrabStrength);
				if(h.GrabStrength >= 0.9f){
					isGrab = true;
				}
				else{
					isGrabOld = isGrab;
					isGrab = false;
				}
			}
		}

		if(!isGrabOld && isGrab && isEnd == false){
			isEnd = true;
			vm.Play(0);
			Application.LoadLevelAsync("play");
		}
	}
}
