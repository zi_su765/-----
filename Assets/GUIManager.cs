﻿using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {

	
	public Texture wave;
	public Rect waveRect;

	public Texture waveNumTex;
	public Rect waveNumRect;
	public Rect waveNumTexRect;
	public int waveNum;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		waveNumTexRect.x = (waveNumTex.width - (10 - waveNum) * waveNumRect.width) / waveNumTex.width;
	}

	void OnGUI(){
		GUI.DrawTexture(waveRect, wave);

		GUI.DrawTextureWithTexCoords(waveNumRect, waveNumTex, waveNumTexRect);
	}
}
