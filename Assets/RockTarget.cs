﻿using UnityEngine;
using System.Collections;

public class RockTarget : MonoBehaviour {

	public GameObject targetEnemy;
	public AnimationCurve scaleCurve;
	public Vector3 offset;
	Vector3 originalScale;
	float scaleTime;
	// Use this for initialization
	void Start () {
		gameObject.transform.position = targetEnemy.transform.position;
		originalScale = transform.localScale;
		scaleTime = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		scaleTime += Time.deltaTime;
		gameObject.transform.localScale = originalScale * scaleCurve.Evaluate(scaleTime);
		gameObject.transform.position = targetEnemy.transform.position + offset;
	}
}
