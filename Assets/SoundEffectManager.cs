﻿using UnityEngine;
using System.Collections;

public class SoundEffectManager : MonoBehaviour {

	public AudioSource[] sources;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public AudioSource GetAudio(int index){
		return Instantiate(sources[index]) as AudioSource;
	}
}
