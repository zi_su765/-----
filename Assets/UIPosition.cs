﻿using UnityEngine;
using System.Collections;

public class UIPosition : MonoBehaviour {

	Vector3 pos;
	public Vector3 offset;
	// Use this for initialization
	void Start () {
		pos = new Vector3(-Screen.width/2 , Screen.height/2, 0.0f);
		transform.localPosition = pos;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
