﻿using UnityEngine;
using System.Collections;

public class WaveEffect : MonoBehaviour {

	public AnimationCurve curve;

	float animTime;

	public bool IsAnimStart{
		get;
		set;
	}

	EnemyGenerater eg;
	GameMaster gm;
	// Use this for initialization
	void Start () {
		animTime = 0.0f;
		float e = curve.Evaluate(animTime);
		float texw = GetComponent<UITexture>().width/2;
		Vector3 pos = transform.localPosition;
		pos.x = Screen.width * e + texw *2.0f* e + (-Screen.width * (1.0f - e) - (texw * 2.0f * (1.0f - e)));
		transform.localPosition = pos;
		eg = GameObject.FindObjectOfType<EnemyGenerater>();
		gm = GameObject.FindObjectOfType<GameMaster>();
	}

	public void StartAnimation(){
		animTime = 0.0f;
		IsAnimStart = true;
	}
	// Update is called once per frame
	void Update () {
		if(IsAnimStart){
			animTime += Time.deltaTime * 0.3f;
			CutInAnimation();
			if(animTime > 1.0f){
				IsAnimStart = false;
				eg.Init();
			}
		}
	}

	void CutInAnimation(){
		float e = curve.Evaluate(animTime);
		Vector3 pos = transform.localPosition;
		float texw = GetComponent<UITexture>().width/2;
		//pos.x = Screen.width * e - Screen.width/2 - texw + (texw* 2 * e);
		pos.x = Screen.width * e + texw *2.0f* e + (-Screen.width * (1.0f - e) - (texw * 2.0f * (1.0f - e)));
		transform.localPosition = pos;
	}
}
