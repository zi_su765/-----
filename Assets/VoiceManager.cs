﻿using UnityEngine;
using System.Collections;

public class VoiceManager : MonoBehaviour {

	public AudioClip[] clips;
	public GameObject voiceOnce;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Play(int index){
		GameObject v = Instantiate(voiceOnce) as GameObject;
		v.GetComponent<AudioSource>().clip = clips[index];
		v.GetComponent<AudioSource>().Play();
		DontDestroyOnLoad(v);
		Destroy(v, clips[index].length);

		//audio.PlayOneShot(clips[index]);
		//DontDestroyOnLoad(gameObject);
	}
}
