﻿using UnityEngine;
using System.Collections;

public class RockOn : MonoBehaviour {

	public float rotateSpeed;
	float rotAngle;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		rotAngle += rotateSpeed;
		if(rotAngle > 360.0f){
			rotAngle = 0.0f;
		}
		Vector3 dir = gameObject.transform.position - Camera.main.transform.position;
		dir = dir.normalized;
		transform.rotation = Quaternion.LookRotation(dir) * Quaternion.AngleAxis(270.0f, Vector3.right);
		transform.rotation *= Quaternion.AngleAxis(rotAngle, Vector3.up);
	}
}
