﻿using UnityEngine;
using System.Collections;

public class FireBall : MonoBehaviour {

	Vector3 vel;
	public float speed;
	public bool IS_THROW{
		get;
		set;
	}

	public float deathWaitTime;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += (vel * speed * Time.deltaTime);
		if(IS_THROW){
			deathWaitTime -= Time.deltaTime;
		}

		if(deathWaitTime < 0.0f){
			Destroy(gameObject);
		}
	}

	public void SetVelocity(Vector3 vel){
		this.vel = vel;
	}

	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Enemy" && IS_THROW){
			Destroy(gameObject);
		}
	}
}
