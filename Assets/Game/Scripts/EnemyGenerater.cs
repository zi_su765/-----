﻿using UnityEngine;
using System.Collections;

public class EnemyGenerater : MonoBehaviour {

	public GameObject[] enemies;

	public float enemyAppearTime;
	float appearTime;
	public bool GenerateFlag;
	public int GenerateMax;

	int generateNum;
	int deadEnemyNum;

	public float radius;
	public bool IsWaveEnd;

	WaveEffect wf;
	RespawnPoint[] respawnPoints;
	GameMaster gm;
	public int WAVE_NUM{
		get;
		set;
	}
	// Use this for initialization
	void Start () {
		Init ();
		respawnPoints = GameObject.FindObjectsOfType<RespawnPoint>();
		wf = GameObject.FindObjectOfType<WaveEffect>();
		gm = GameObject.FindObjectOfType<GameMaster>();
		WAVE_NUM = 0;
	}

	public void Init(){
		generateNum = 0;
		deadEnemyNum = 0;
		GenerateFlag = true;
		IsWaveEnd = false;
	}
	// Update is called once per frame
	void Update () {
		if(GenerateFlag && gm.IsClear == false){
			if(generateNum < GenerateMax){
				appearTime -= Time.deltaTime;
				if(appearTime < 0.0f){
					Vector3 pos = gameObject.transform.position;
					Vector3 posdir = Vector3.forward;
					float angle = Random.Range(-60.0f, 60.0f);
					posdir = Quaternion.AngleAxis(angle, Vector3.up) * posdir;
					float r = Random.Range (5.0f, radius);
					posdir *= r;
					pos.x += posdir.x;
					pos.z += posdir.z;

					//
					int respawnIndex = Random.Range(0,respawnPoints.Length);
					RespawnPoint respoint = respawnPoints[respawnIndex];
					if(respoint.Respawaning){
						for(int i = 0 ; i < respawnPoints.Length ; i++){
							if(respawnPoints[i].Respawaning == false){
								respoint = respawnPoints[i];
								break;
							}
						}
					}
					if(respoint.Respawaning == false){
						pos = respoint.transform.position;
						pos.y = Terrain.activeTerrain.SampleHeight(pos);
						respoint.Respawaning = true;
						//
						int eneType = Random.Range(0, enemies.Length);
						GameObject enemy = Instantiate(enemies[eneType]) as GameObject;
						enemy.GetComponent<Enemy>().RespawnPointIndex = respawnIndex;
						
						enemy.transform.position = pos;
						Vector3 dir = enemy.transform.position - Camera.main.transform.position;
						enemy.transform.rotation = Quaternion.LookRotation(dir);
						generateNum++;
					}
					appearTime = enemyAppearTime;
				}
			}
		}
	}

	void DeadEnemy(){
		Debug.Log ("Dead");
		deadEnemyNum++;
		if(deadEnemyNum == GenerateMax){
			IsWaveEnd = true;
			WAVE_NUM++;
			if(WAVE_NUM >= gm.WAVE_MAX){
				Debug.Log("WaveNum:"+WAVE_NUM);
				gm.IsClear = true;
			}
			if(gm.IsClear == false){
				wf.StartAnimation();
			}

		}
	}

}
