﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	Animator anim;
	AnimatorStateInfo state;
	public GameObject rockOn;
	GameObject rockOnClone;
	bool isVisibleCamera;
	public float rotPos = 1.0f;
	bool isDeath = false;
	RespawnPoint[] respawnPoints;
	public int RespawnPointIndex{
		get;
		set;
	}

	public bool IsDeath{
		get{return isDeath;}
	}

	enum MOVEMODE{
		NONE,
		PINGPONG,
	}

	MOVEMODE moveMode;
	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponentInChildren<Animator>();
		state = anim.GetCurrentAnimatorStateInfo(0);
		isVisibleCamera = false;
		moveMode = MOVEMODE.NONE;
		respawnPoints = GameObject.FindObjectsOfType<RespawnPoint>();
	}
	
	// Update is called once per frame
	void Update () {
		float height = Terrain.activeTerrain.SampleHeight(transform.position);
		Vector3 pos = transform.position;
		pos.y = height;
		transform.position = pos;

		Vector3 dir = transform.position - Camera.main.transform.position;

		//MoveMethor
		Move();

		//RockOn/Off method
		Vector3 screenPos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		if((screenPos.x > 0.0f && screenPos.x < Screen.width)
		   && (screenPos.y > 0.0f && screenPos.y < Screen.height)
		   && isVisibleCamera == false
		   && isDeath == false
		  ){
			isVisibleCamera = true;
			rockOnClone = Instantiate(rockOn) as GameObject;
			rockOnClone.GetComponent<RockTarget>().targetEnemy = gameObject;
			rockOnClone.gameObject.transform.parent = gameObject.transform;
		}
		if(((screenPos.x < 0.0f || screenPos.x > Screen.width)
		   || ( screenPos.y < 0.0f || screenPos.y > Screen.height))
		   && isVisibleCamera == true
		   ){
			isVisibleCamera = false;
			Destroy(rockOnClone);
		}
	}
	
	void Move(){
		Vector3 pos = transform.position;
		pos.y = Terrain.activeTerrain.SampleHeight(pos);
		transform.position = pos;
		if(moveMode == MOVEMODE.NONE){
		}
		else if(moveMode == MOVEMODE.PINGPONG){
			Vector3 dir = transform.position - Camera.main.transform.position;
			pos = Quaternion.AngleAxis(rotPos, Vector3.up) * dir;
			transform.position = pos;
			transform.rotation = Quaternion.LookRotation(dir.normalized);
		}
	}

	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "FireBall" && isDeath == false){
			if(collider.gameObject.GetComponent<FireBall>().IS_THROW){
			isDeath = true;
			Destroy(gameObject.GetComponent<SphereCollider>());
			GameObject.FindObjectOfType<EnemyGenerater>().SendMessage("DeadEnemy");
			Animator anim = GetComponentInChildren<Animator>(); 
			anim.SetTrigger("death");

			CreateHitEffect();

			RockTarget target = GetComponentInChildren<RockTarget>();
			if(target != null){
				Destroy (target.gameObject);
			}
			Destroy(gameObject, 3.0f);
			}
		}
	}

	void CreateHitEffect(){
		GameObject em = GameObject.Find ("EffectManager");
		GameObject hitEffect = em.GetComponent<EffectManager>().effects[(int)EffectManager.EffectType.FIREWALL];
		Instantiate(hitEffect, gameObject.transform.position + Vector3.up, Quaternion.AngleAxis(-90.0f, Vector3.right));
		//GameObject seManager = GameObject.Find ("SoundEffectManager");
		//AudioSource se = seManager.GetComponent<SoundEffectManager>().GetAudio(0);
		//se.PlayOneShot(se.clip);
		//Destroy(se.gameObject, se.clip.length);
	}

	void OnDestroy(){
		respawnPoints[RespawnPointIndex].Respawaning = false;
	}
}
