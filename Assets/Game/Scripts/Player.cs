﻿using UnityEngine;
using System.Collections;
using Leap;
public class Player : MonoBehaviour {

	public Vector3 offset;
	public HandController hand;
	public GameObject[] effects;

	enum EFFECT_TYPE{
		Fire,
	}

	private GameObject[] fingerFlares;
	bool isHandOpen;
	bool isHandOpenOld;
	bool isThrowFire;
	public AudioSource fireSound;
	AudioSource fs;
	public AnimationCurve volumeCurve;
	float volumeTime;
	float handCloseTime;
	// Use this for initialization
	void Start () {
		hand = GetComponentInChildren<HandController>();
		fingerFlares = new GameObject[effects.Length];
		isHandOpenOld = true;
		isHandOpen = true;
		isThrowFire = true;
		volumeTime = 0.0f;
		fs = null;
	}
	
	// Update is called once per frame
	void Update () {
		HandModel[] models = hand.GetAllGraphicsHands();
		foreach(HandModel h in models){
			int index = 0;
			float phoenixTime = 0.0f;
			if(h.GetLeapHand().GrabStrength < 0.1f ){
				isHandOpenOld = isHandOpen;
				isHandOpen = true;
				phoenixTime = Time.time - handCloseTime;
			}
			if(h.GetLeapHand().GrabStrength >= 0.5f){
				isHandOpenOld = isHandOpen;
				isHandOpen = false;
				handCloseTime = Time.time;
			}
			if(isHandOpenOld == false && isHandOpen == true && isThrowFire == true){
				for(int i = 0 ; i < fingerFlares.Length ; i++){
					fingerFlares[i] = null;
					fingerFlares[i] = Instantiate(effects[(int)EFFECT_TYPE.Fire]) as GameObject;
					isThrowFire = false;

				}
				fs = Instantiate(fireSound) as AudioSource;
				fs.PlayOneShot(fs.clip);
				Destroy(fs.gameObject, fs.clip.length);
			}

			foreach(FingerModel f in h.fingers){
				if(fingerFlares[index] != null && !fingerFlares[index].GetComponent<FireBall>().IS_THROW){
					fingerFlares[index].transform.position = f.GetTipPosition();
				}
				index++;
			}
		}
		if(fs != null){
			fs.volume = volumeCurve.Evaluate(volumeTime);
			volumeTime += Time.deltaTime;
			if(fs.volume <= 0.0f){
				volumeTime = 0.0f;
				Destroy(fs.gameObject);
			}
		}

		if(models.Length == 0){
			for(int i = 0 ; i < fingerFlares.Length ; i++){
				if(fingerFlares[i] != null){
					if(!fingerFlares[i].GetComponent<FireBall>().IS_THROW){
						FireBall fb = fingerFlares[i].GetComponent<FireBall>();
						fb.SetVelocity(Camera.main.transform.rotation * Vector3.forward);
						fb.IS_THROW = true;
						isThrowFire = true;
					}
				}
			}
			isThrowFire = true;
		}
		Frame frame = hand.GetFrame();
		if(frame != null){
			foreach(Gesture g in frame.Gestures()){
				if(g.Type == Gesture.GestureType.TYPE_SWIPE){
					RockTarget[] targets = GameObject.FindObjectsOfType<RockTarget>();
					int targetIndex = 0;
					if(fingerFlares != null){
						foreach(GameObject f in fingerFlares){
							if(f == null){
								break;
							}
							if(targetIndex > 4){
								break;
							}

							FireBall fb = f.GetComponent<FireBall>();
							if(targets.Length == 0){
								fb = f.GetComponent<FireBall>();
								fb.SetVelocity(Camera.main.gameObject.transform.rotation * Vector3.forward);
							}
							else{
								Vector3 dir = (targets[targetIndex].transform.position - f.transform.position).normalized;
								Vector3 eyeDir = Camera.main.transform.rotation * Vector3.forward;
								float rad = Vector3.Dot(dir, eyeDir);
								if(rad < 0){
									fb = f.GetComponent<FireBall>();
									fb.SetVelocity(Camera.main.gameObject.transform.rotation * Vector3.forward);
								}
								else{
									fb.SetVelocity(dir);
								}
							}
							f.GetComponent<FireBall>().IS_THROW = true;
							isThrowFire = true;
							if(targetIndex < targets.Length-1){
								targetIndex++;
							}
						}
					}
				}
			}
		}

		Vector3 pos = transform.position;
		float posy = Terrain.activeTerrain.SampleHeight(pos);
		pos.y = posy;
		transform.position = pos + offset;
		RotateEye();
	}

	void RotateEye(){
		if(Input.GetKey(KeyCode.A)){
			transform.rotation *= Quaternion.AngleAxis(-1.0f, Vector3.up);
		}
		else if(Input.GetKey(KeyCode.D)){
			transform.rotation *= Quaternion.AngleAxis(1.0f, Vector3.up);
		}
	}
}
