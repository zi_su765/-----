Shader "Custom/RockOn" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
}
SubShader {
    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
    LOD 200

    // デプスバッファのみにレンダリングする追加のパス
    Pass {
    	Cull Off
        ZTest Always
        ColorMask 0
    }

    // forward renderingパスをTransparent/Diffuseから渡します
    UsePass "Unlit/Transparent/Diffuse/FORWARD"
}
Fallback "Unlit/Transparent"
}