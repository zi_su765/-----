﻿using UnityEngine;
using System.Collections;

public class BGMManager : MonoBehaviour {

	public AudioClip[] clips;
	// Use this for initialization
	void Start () {
		Play (0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Play(int index){
		audio.Stop();
		audio.clip = clips[index];
		audio.Play();
	}
}
