﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {

	public int WAVE_MAX{
		get;
		private set;
	}

	public bool IsClear{
		get;
		set;
	}
	public int WAVE_NOW;
	public GameObject enemyGenerater;
	public GameObject waveNum;
	public GameObject clearCutIn;
	EnemyGenerater eg;
	VoiceManager vm;
	bool isGameOver;
	// Use this for initialization
	void Start () {
		WAVE_MAX = 3;
		eg = enemyGenerater.GetComponent<EnemyGenerater>();
		isGameOver = false;
		vm = GameObject.FindObjectOfType<VoiceManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if(eg.IsWaveEnd){
			Rect uv = waveNum.GetComponent<UITexture>().uvRect;
			uv.x = eg.WAVE_NUM / 10.0f;
			waveNum.GetComponent<UITexture>().uvRect = uv;
		}

		if(IsClear && isGameOver == false){
			Debug.Log ("Clear!!!");
			Instantiate(clearCutIn);
			isGameOver = true;
		}


		if(Input.GetKeyDown(KeyCode.Space)){
			vm.Play(3);
			Application.LoadLevelAsync("title");
		}
	}

}
